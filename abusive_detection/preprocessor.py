import re

import pandas as pd
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory


def preproc(sentence):
    # remove non char
    x = sentence
    x = x.lower()
    x = re.sub(r"[^.,a-zA-Z0-9 \n\.]", " ", x)  # remove symbol
    x = re.sub("[\s]+", " ", x)  # remove additional whitespace
    x = re.sub(r"[^\w\s]", "", x)  # remove punctuation
    x = x.strip()  # remove any space at the start and the end

    # remove stopwords
    stop_factory = StopWordRemoverFactory()
    more_stopword = ["nya", "yg", "aja", "pun", "mas", "mbak"]
    remove_stopword = [
        "bisa",
        "boleh",
        "nggak",
        "ok",
        "seharusnya",
        "setidaknya",
        "tapi",
        "tidak",
        "tolong",
    ]
    data_stopwords = stop_factory.get_stop_words() + more_stopword

    token = x.split(" ")
    for words in remove_stopword:
        data_stopwords.remove(words)

    x = [word for word in token if word not in data_stopwords]

    # remove kata alay
    alay = pd.read_csv(
        "https://raw.githubusercontent.com/okkyibrohim/id-abusive-language-detection/master/kamusalay.csv"
    )
    alay.columns = ["alay", "normal"]
    result = []
    for word in x:
        if word not in list(alay["alay"]):
            result.append(word)
        else:
            # print(list(alay[alay["alay"] == word]["normal"])[0])
            result.append(list(alay[alay["alay"] == word]["normal"])[0])
    result = " ".join(result)
    return result


# print(preproc("saya kamu kita akoh"))
