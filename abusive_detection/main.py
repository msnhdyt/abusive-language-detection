import json

import numpy as np
import requests
import tensorflow as tf
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel, validator
from transformers import AutoTokenizer

from abusive_detection.preprocessor import preproc


class PredictInput(BaseModel):
    text: str

    @validator("text")
    def text_must_not_have_empty(cls, v):
        if v == "":
            raise ValueError("input can't be empty")
        return v


app = FastAPI()
tokenizer = AutoTokenizer.from_pretrained(
    "w11wo/indonesian-roberta-base-sentiment-classifier"
)


@app.get("/")
def root():
    return {"message": "OK"}


@app.post("/predict")
def predict(data: PredictInput):
    sentence = preproc(data.text)

    # Tokenize the sentence
    batch = tokenizer(sentence)

    # Convert the batch into a proper dict
    batch = dict(batch)

    # Put the example into a list of size 1, that corresponds to the batch size
    batch = [batch]

    # The REST API needs a JSON that contains the key instances to declare the examples to process
    input_data = {"instances": batch}

    # Query the REST API, the path corresponds to http://host:port/model_version/models_root_folder/model_name:method
    path = "http://192.168.99.100:8501"
    r = requests.post(
        path + "/v1/models/indonesian-roberta:predict",
        data=json.dumps(input_data),
    )
    print(r.text)
    # print(r.text["predictions"])
    json_result = json.loads(r.text)
    label = int(np.argmax(json_result["predictions"][0]))
    label_text = None
    if label == 0:
        label_text = "abusive"
    elif label == 1:
        label_text = "abusive, not offensive"
    else:
        label_text = "offensive"
    confidence_score = float(np.max(tf.nn.softmax(json_result["predictions"][0])))
    result = {"text": sentence, "label": label_text, "confidence": confidence_score}
    # result = None
    # result = json.dumps(result)
    return result


def start():
    uvicorn.run("abusive_detection.main:app", host="0.0.0.0", port=8000, reload=True)
